﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using Moor.XmlConversionLibrary.XmlToCsvHelpers;
using Moor.XmlConversionLibrary.XmlToCsvStrategy;
using System.Xml.Serialization;
using System.Configuration;
using System.Text.RegularExpressions;

namespace SIEBEL.XMLtoCSV
{
    class Program
    {
        static void Main(string[] args)
        {
            int i;

            if (args.Length >= 1 && Int32.TryParse(args[0], out i) && File.Exists(Environment.CurrentDirectory + @"\Config.csv"))
            {
                StreamReader sr = new StreamReader(Environment.CurrentDirectory + @"\Config.csv");
                string sSetting = sr.ReadToEnd();
                sr.Close();
                string sConfig = Regex.Split(sSetting, Environment.NewLine)[Convert.ToInt32(args[0])];

                string inputMap = string.Empty;
                string inputMask = string.Empty;
                string outputMap = string.Empty;
                string outputFileName = string.Empty;
                string archiveMap = string.Empty;
                string delimiter = string.Empty;
                string prefix = string.Empty;

                for (int a = 0; a < sConfig.Split('~').Length; a++)
                {
                    switch (a)
                    {
                        case 0:
                            inputMap = sConfig.Split('~')[a];
                            break;
                        case 1:
                            inputMask = sConfig.Split('~')[a];
                            break;
                        case 2:
                            outputMap = sConfig.Split('~')[a];
                            break;
                        case 3:
                            outputFileName = sConfig.Split('~')[a];
                            break;
                        case 4:
                            archiveMap = sConfig.Split('~')[a];
                            break;
                        case 5:
                            delimiter = sConfig.Split('~')[a];
                            break;
                        case 6:
                            prefix = sConfig.Split('~')[a];
                            break;
                    }
                }



                foreach (string file in Directory.GetFiles(inputMap, inputMask))
                {
                    FileInfo fi = new FileInfo(file);
                    string sDate = DateTime.Now.ToString("yyyyMMddHHmmssfff");
                    var converter = new XmlToCsvUsingDataSet(fi.FullName);
                    var context = new XmlToCsvContext(converter);
                    


                    outputFileName = outputFileName.Replace("[date]",DateTime.Now.ToString("yyyyMMddHHmmssfff"));
                    outputFileName = outputFileName.Replace("[inputfile]",Path.GetFileNameWithoutExtension(fi.Name));
                    string sExportFile = outputMap + @"\" + outputFileName;


                    foreach (string xmlTableName in context.Strategy.TableNameCollection)
                    {
                        if (xmlTableName != "header")
                        {
                            context.Execute(xmlTableName, sExportFile+"tmp", Encoding.Unicode);
                            StreamReader sr2 = new StreamReader(sExportFile + "tmp");
                            string s = sr2.ReadToEnd();
                            sr2.Close();
                            s = s.Replace(",", delimiter);
                            s = s.Replace("\"", "");
                            
                            StreamWriter sw = new StreamWriter(sExportFile);
                            foreach (string sLijn in Regex.Split(s, Environment.NewLine))
                            {
                                if(sLijn.Trim().Length>0)
                                    sw.Write(prefix+delimiter+sLijn+Environment.NewLine);
                            }
                            sw.Close();

                            File.Delete(sExportFile + "tmp");

                            
                        }
                    }
                    File.Move(fi.FullName, archiveMap + "\\" + Path.GetFileNameWithoutExtension(fi.Name) + sDate + fi.Extension);
                }


            }
            else
            {
                Console.WriteLine("Ongeldige parameters");
            }
            

           



            //XmlSerializer ser = new XmlSerializer(typeof(aankomstBevestiging));
            //aankomstBevestiging cars;
            //using (XmlReader reader = XmlReader.Create(@"C:\Users\pdoo\Documents\Klanten\Wkamp\000000175EtamaankomstBevestiging.xml"))
            //{
            //    cars = (aankomstBevestiging)ser.Deserialize(reader);
               
            //}
            

            
            }
    }
}
